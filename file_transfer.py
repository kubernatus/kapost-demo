#!/usr/bin/python
import boto3, os, sys

# Set variables from environmental variables from Dockerfile
first_bucket  = os.environ['first_bucket']
second_bucket = os.environ['second_bucket']
mb_size        = float(os.environ['mb_size'])

resource = boto3.resource('s3')
bucket   = resource.Bucket(first_bucket)
files_to_copy = []

# Check if the bucket exists or not before proceeding
for check_bucket in (first_bucket, second_bucket):
  try:
    results = resource.Bucket(check_bucket) in resource.buckets.all()
    if results == False:
        raise ValueError()
  except (ValueError):
    error_message = "The bucket {} does not exists.".format(check_bucket)
    print(error_message)
    sys.exit()

def copy_to_bucket(bucket_from_name, bucket_to_name, file_name):
    copy_source = {
        'Bucket': bucket_from_name,
        'Key': file_name
    }
    resource.Object(bucket_to_name, file_name).copy(copy_source)

def convert_bytes(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f" % (num)
        num /= 1024.0

for key in bucket.objects.all(): 
    size = convert_bytes(key.size)
    size = float(size)
    if (size > mb_size):
        files_to_copy.append(key.key)

for i in files_to_copy:
    copy_to_bucket(first_bucket, second_bucket, i)