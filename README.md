# S3 File Transfer

This script will allow you to easily transfer over all files that exceed a certain MB limit between two S3 buckets.

## Local Build and Execution

You will first need to create the buckets by applying the terraform in the terraform directory:

```
terraform plan
terraform apply 
```

You will then need to upload all of the dummy files into the first bucket:

```
aws s3 cp dummy-files/ s3://kapost-demo --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --recursive
```

In order to execute the file locally, you will need to build the Docker image on your local machine. For local execution I utilized [iam-docker-run](https://pypi.org/project/iam-docker-run/) and configured my S3 credentials in ~/.aws/config.

```
docker build . -t python/kapost
iam-docker-run --image python/kapost --profile kapost
```