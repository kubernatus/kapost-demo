provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "first_bucket" {
  bucket = "${var.first_bucket}"
  acl = "private"
  
  policy = <<POLICY
{
    "Id": "Policy1549486814105",
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Stmt1549486807478",
        "Action": [
          "s3:Put*",
          "s3:Get*"
        ],
        "Effect": "Allow",
        "Resource": "arn:aws:s3:::${var.first_bucket}/*",
        "Principal": {
          "AWS": [
            "arn:aws:iam::${var.aws_account}:root"
          ]
        }
      }
    ]
}
POLICY

}

resource "aws_s3_bucket" "second_bucket" {
  bucket = "${var.second_bucket}"
  acl = "private"
  
  policy = <<POLICY
{
    "Id": "Policy1549486814105",
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "Stmt1549486807478",
        "Action": [
          "s3:Put*",
          "s3:Get*"
        ],
        "Effect": "Allow",
        "Resource": "arn:aws:s3:::${var.second_bucket}/*",
        "Principal": {
          "AWS": [
            "arn:aws:iam::${var.aws_account}:root"
          ]
        }
      }
    ]
}
POLICY

}