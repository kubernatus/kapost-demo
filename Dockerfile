FROM python:3.6-alpine
MAINTAINER Brian Johnson
 
RUN pip install awscli && \
    pip install --upgrade boto3

WORKDIR /usr/src/app
COPY file_transfer.py /usr/src/app

# Define the environmental variables to be passed
# first_bucket is the bucket we're copying from.
# second_bucket is the bucket we're copying to.
# mbSize is the minimum file size needed to be copied over.
 
ENV first_bucket="kapost-demo"
ENV second_bucket="kapost-demo2"
ENV mb_size=3

ENTRYPOINT [ "python", "file_transfer.py"]